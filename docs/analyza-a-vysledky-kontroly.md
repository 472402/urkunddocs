Analýza a výsledky kontroly
===========================

Výsledky analýzy odevzdaného souboru systémem Urkund se v rámci ELFu
zobrazují v podobě *Skóre podobnosti* a *Zprávy o podobnosti*. Obě
možnosti jsou podrobně popsány níže.






    1.  [**2.1** Informace o nahraném
        dokumentu](#TOC-Informace-o-nahran-m-dokumentu)
    2.  [**2.2** Seznam zdrojů a nalezených
        shod](#TOC-Seznam-zdroj-a-nalezen-ch-shod)
    3.  [**2.3** Funkční panel](#TOC-Funk-n-panel)
    4.  [**2.4** Text dokumentu a odpovídajících
        zdrojů](#TOC-Text-dokumentu-a-odpov-daj-c-ch-zdroj-)

### Skóre podobnosti

Výsledky analýzy jsou dostupné přímo v ELFu. V rámci aktivity typu Úkol
se z pohledu učitele zobrazují přímo v tabulce s přehledem všech
odevzdaných úkolů.

![](analyza-a-vysledky-kontroly/urkund_vysledky_ukol_ucitel.png)

Obr. 1: Zobrazení výsledků analýzy systémem Urkund v úkolu z pohledu
učitele.


Z pohledu studenta se pak výsledky analýzy zobrazují na stránce s
odevzdaným úkolem (pokud vyučující v nastavení povolil, aby se výsledky
studentům zobrazovaly) včetně odkazu na vyřazení souboru z databáze
zdrojů.

![](analyza-a-vysledky-kontroly/urkund_vysledky_ukol_student.png)

Obr. 2: Zobrazení výsledků analýzy systémem Urkund v úkolu z pohledu
studenta.



V rámci diskuzního fóra se pak výsledky zobrazují pod každým jednotlivým
diskuzním příspěvkem.

![](analyza-a-vysledky-kontroly/urkund_vysledky_forum.png)

Obr. 3: Zobrazení výsledků analýzy systémem Urkund v rámci diskuzního
fóra.


Z pohledu učitele se vždy zobrazí jak skóre podobnosti, tak i odkaz na
kompletní zprávu o podobnosti (viz níže). Studentům se ve výchozím
nastavení zobrazuje pouze skóre o podobnosti. Vyučující však může
nastavit, aby se studentům zobrazovala i kompletní zpráva o podobností
(viz stránku [Nastavení systému Urkund v
ELFu](/urkunddocs/nastaveni-systemu-urkund-v-elfu)). Vedle skóre
podobnosti se studentům zobrazuje vždy také odkaz na vyřazení daného
souboru s databáze zdrojů.



Při zobrazování výsledků analýzy používá nástroj Urkund vedle číselného
vyjádření také **barevné kódování**. U prací s žádnou či nízkou mírou
podobnosti je výsledné skóre zobrazeno na zeleném či žlutém podkladu.
Střední míra podobnosti je vyjádřena oranžovou barvou a práce s vysokou
či úplnou mírou podobnosti jsou zobrazeny červeně resp. černě.
Přinejmenším u prací označených červeně a černě je vždy vhodné zobrazit
si také kompletní zprávu o podobnosti (viz níže).

![](analyza-a-vysledky-kontroly/urkund_barevne_kodovani.png)

Obr. 4: Ukázka barevného kódování zobrazeného skóre podobnosti.


**Pozor!** Rozhodnutí o tom, zda jde o plagiát či nikoli, musí vždy
provést vyučující po důkladné kontrole textu. Ani v případě systému
Urkund se nelze spolehnout pouze na výsledné skóre podobnosti. Systém
Urkund vyhledává pouze podobnosti mezi texty, čímž může kontrolu práce
usnadnit. Posouzení, zda jde o korektní práci s citacemi či plagiát však
musí provést vyučující.

### Zpráva o podobnosti

Zpráva o podobnosti obsahuje informace o dokumentu, který byl poslán ke
kontrole systémem Urkund, a zobrazuje kompletní výsledky provedené
analýzy. Nejde však pouze o statický přehled. Zpráva o podobnosti má
podobu interaktivního nástroje, který dovoluje s analyzovaným dokumentem
dále pracovat a ověřovat konkrétní pasáže textu, u kterých může být na
základě podobnosti s jiným zdrojem podezření z plagiátorství.

Na stránkách systému Urkund si můžete zobrazit [ukázku zprávy o
podobnosti](https://secure.urkund.com/view/17982142-422526-495118){:target="_blank"}, na
které se můžete seznámit se strukturou zprávy o podobnosti a vyzkoušet
si její různé funkcionality. Celkově můžeme zprávu o podobnosti rozdělit
do čtyř základních sekcí (viz obrázek níže):

1.  Informace o nahraném dokumentu
2.  Seznam zdrojů a nalezených shod
3.  Funkční panel
4.  Text dokumentu a odpovídajících zdrojů

![](analyza-a-vysledky-kontroly/urkund_zprava_o_podobnosti.png)

Obr. 5: Základní rozvržení jednotlivých sekcí zprávy o podobnosti.


Fungování jednotlivých sekcí Zprávy o podobnosti je pak podrobněji
popsáno níže.

#### Informace o nahraném dokumentu

Sekce vlevo nahoře zobrazuje přehled základních informací o nahraném
dokumentu.

![](analyza-a-vysledky-kontroly/urkund_sekce_informace_o_dokumentu.png)

Obr. 6: Sekce s informacemi o nahraném dokumentu vlevo nahoře.


V sekci s informacemi o nahraném dokumentu najdete následující prvky:

-   **Originál dokumentu**, který byl odeslán ke kontrole systémem
    Urkund. Kliknutím na odkaz můžete dokument stáhnout v jeho původní
    podobě.
-   **Čas přijetí** daného dokumentu systémem Urkund.
-   **Odesilatel dokumentu.** Zde se zobrazuje e-mailová adresa (příp. i
    jméno) uživatele, který daný dokument do systému Urkund odeslal. V
    případě, že byl dokument ke kontrole odeslán skrze kurzy v systému
    ELF, bude se zde zobrazovat e-mailová adresa v podobě
    *UČO@mail.muni.cz*. V opačném případě zde může být zobrazena
    libovolná adresa uživatele, který dokument ke kontrole odeslal.
-   **Příjemce dokumentu.** Zde se zobrazuje adresa systému Urkund,
    která slouží ke kontrole dokumentů. V případě, že byl dokument ke
    kontrole odeslán skrze kurzy v systému ELF, bude se zde zobrazovat
    stejné adresa, která je uvedena v nastavení dané aktivity v ELFu
    (tj. Úkol, Fórum či Workshop). Adresa systému Urkund sloužící k
    zasílání dokumentů ke kontrole má podobu
    *xxx@analysis.urkund.com*.
-   **Zpráva.** V případě, že k odesílanému dokumentu byla připojena
    zpráva, můžete si je zde zobrazit. U dokumentů odesílaných skrze
    kurzy v systému ELF zpráva obvykle obsažena není. Při odesílání
    dokumentů ke kontrole pomocí e-mailu se do pole zpráva vloží text
    e-mailu.
-   **Informace o podobnosti** obsahují jednak skóre podobnosti včetně
    výše zmiňovaného *barevného kódování*, jednak přibližnou délku
    dokumentu a počet zdrojů, které systém Urkund odhalil.

**Pozor!** Skóre podobnosti zobrazované ve zprávě o podobnosti se
dynamicky mění podle toho, jaké zdroje jsou vybrány v sekci *Seznam
zdrojů a nalezených shod*. Ve výchozím zobrazení jsou vybrány všechny
zdroje, tudíž se zobrazuje skóre podobnosti vypočítané na základě všech
nalezených zdrojů. Můžete ale nechat vybrané jen určité zdroje (např.
proto, že některé zdroje student cituje korektně), čímž se skóre
podobnosti přepočítá jen na základě vybraných zdrojů. Přepočítané skóre
se ovšem zobrazuje vždy jen ve zprávě o podobnosti, v ELFu se vždy
zobrazuje jen výchozí skóre podobnosti.

#### Seznam zdrojů a nalezených shod 

V pravé horní části se nachází sekce zobrazující kompletní seznam
zdrojů, ve kterých byly nalezeny stejné či dostatečně podobné úryvky
texty, jako v dokumenty poslaném na kontrolu. Se seznamem zdrojů a
nalezených shod můžete pracovat ve dvou různých režimech.

První možností je zobrazit si záložku **Sources**, která zobrazuje
seznam zdrojů seřazených podle toho, jak velké množství textu je stejné
s kontrolovaným dokumentem. Čím více je z daného zdroje přebráno textu,
tím výše se v seznamu zobrazuje. Kliknutím na ikonku se znaménkem plus u
jednotlivých zdrojů si pak můžete zobrazit seznam všech úryvků z daného
zdroje, u kterých je nalezena dostatečná shoda s kontrolovaným
dokumentem.

![](analyza-a-vysledky-kontroly/urkund_sekce_sdroje_a_shody_1.png)

Obr. 7: Zobrazení zdrojů podle množství nalezených shodných úryvků.

Druhou možností je pracovat se záložkou **Highlights**, která naopak
zobrazuje seznam konkrétních shodných úryvků, a to v pořadí, jak jdou za
sebou v kontrolovaném dokumentu.

![](analyza-a-vysledky-kontroly/urkund_sekce_sdroje_a_shody_2.png)

Obr. 8: Zobrazení shodných úryvků podle toho, jak následující v
kontrolovaném dokumentu.


Jak na záložce *Sources* tak i na záložce *Highlights* můžete zároveň
zaškrtnutím upravovat výběr zdrojů či úryvků, které se mají zvýrazňovat
v sekci *Text dokumentu a odpovídajících zdrojů* a být zahrnuty do
výpočtu skóre podobnosti.


Některé zdroje a úryvky mohou být zobrazovány šedě. Jde buď o
**alternativní zdroje**, nebo **nevyužité zdroje**. *Alternativní
zdroje* jsou takové, které obsahují shodné úryvky, které již byly
nalezeny v jiném (primárním) zdroji. Určitá pasáž kontrolovaného
dokumentu může být totiž shodná s několika různými zdroji. Systém Urkund
pak vybere jeden se zdrojů jako primární (obvykle ten, který obsahuje
nejvíce stejného textu) a ostatní zdroje, kde se daná pasáž také
objevuje, pak bere jako alternativní zdroje. Mezi *nevyužité zdroje* pak
spadají zdroje, které sice byly využity v rámci analýzy, ale buď
neobsahují dostatečně velké shody, nebo jsou tyto shody již pokryty
alternativními zdroji.

#### Funkční panel

Ve střední části stránky najdete horizontální funkční panel, který
rozděluje stránku se zprávou o podobnosti na horní a dolní část. S
funkčním panelem můžete tažením pohybovat nahoru a dolů, čímž si dle
potřeby upravíte velikost sekcí v horní a dolní části stránky se zprávou
o podobnosti.


Funkční panel obsahuje tři skupiny tlačítek (viz obr 4 výše). Skupina
tlačítek vlevo obsahuje následující čtyři tlačítka, pomocí kterých si
můžete upravovat zobrazení textu dolní v sekci (tj. *Text dokumentu a
odpovídajících zdrojů*):

-   **Zobrazení alternativních shod.** Pokud je nějaký úryvek nalezen ve
    více zdrojích, můžete si v textu nechat vyznačit nejen shodu, kterou
    systém Urkund vybral jako hlavní, nýbrž i další alternativní zdroje
    shodující se s daným úryvkem.
-   **Detailní zobrazení rozdílů.** V rámci nalezených shodných úryvků
    si můžete nechat vyznačit konkrétní jednotlivá slova, která byla
    oproti původnímu zdroji změněna. To může být užitečné v případech,
    kdy není text z původního zdroje přebrán doslova, nýbrž jen mírně
    pozměněn (např. určitá slova jsou vynechána či přidána nebo
    nahrazena synonymními výrazy).

![](analyza-a-vysledky-kontroly/urkund_detailni_rozdily.png)

Obr. 9: Zobrazení detailních rozdílů v textu mezi kontrolovaným
dokumentem (vlevo) a originálním zdrojem (vpravo).

-   **Zvýraznění textu v uvozovkách.** V nahraném dokumentu si můžete
    nechat zvýraznit text uvedený v uvozovkách. Tato funkce můžete
    využít pro rychlou identifikací přímých citací, které se obvykle
    vyznačují právě uvozovkami.
-   **Zvýraznění textu v závorkách.** V nahraném dokumentu si rovněž
    můžete nechat zvýraznit text v závorkách. To může být užitečné např.
    v případech, kdy je využíván citační styl uvádějící odkazy na zdroje
    v závorce přímo v textu namísto v poznámce pod čarou (APA apod.).

Pomocí tlačítek ve středu panelu se můžete pohybovat mezi jednotlivými
úryvky, které systém Urkund označil za problémové (tj. byl schopen je
dohledat v jiných zdrojích).
**
**Skupina tlačítek vpravo pak zahrnuje následující:

-   **Zobrazení varování.** Zde můžete najít přehled upozornění na možné
    problémové aspekty kontrolovaného dokumentu. Jde především o
    situace, kdy může být nějakým způsobem narušen standardní způsob
    analýzy textu odevzdaného dokumentu (např. v textu jsou používány
    různé nestandardní znaky apod.).
-   **Reset.** Tímto tlačítkem vrátíte zprávu o podobnosti do výchozího
    stavu, tj. v sekci *Seznam zdrojů a nalezených shod* se opět vyberou
    všechny nalezené zdroje a v sekci *Informace o nahraném dokumentu*
    se přepočítá skóre podobnosti na výchozí hodnotu započítávající
    všechny nalezené zdroje.
-   **Export zprávy o podobnosti do pdf.** Zde si můžete exportovat
    kompletní zprávu o podobnosti do souboru typu pdf. Do pdf se přitom
    uloží zpráva o podobnosti v aktuálním nastavení. Pomocí tohoto
    tlačítka si tak můžete uložit finální zprávu o podobnosti, kde už
    máte vybrané jen skutečně problémové zdroje, a ve které je
    vypočítané korigované skóre podobnosti jen na základě vybraných
    problémových zdrojů.
-   **Sdílení zprávy o podobnosti.** Tlačítkem pro sdílení můžete na
    zadanou e-mailovou adresu poslat zprávu s odkazem na zprávy o
    podobnosti. Příjemce této zprávy si tak bude moci zobrazit kompletní
    zprávu o podobnosti a interaktivně s ní pracovat.
-   **Odkazy na oficiální nápovědu** k systému Urkund.

#### Text dokumentu a odpovídajících zdrojů

Spodní sekce zprávy o podobnosti zobrazuje jednak samotný text
dokumentu, který byl poslán ke kontrole (vlevo), jednak zdrojové
dokumenty, u kterých byly nalezeny shodné úryvky s kontrolovaným
dokumentem (vpravo).

![](analyza-a-vysledky-kontroly/urkund_sekce_text_dokumentu.png)

Obr. 10: Ukázka zobrazení vyznačených úryvků v textu dokumentu a jejich
odpovídajících zdrojů.


Každý shodný úryvek je barevně vyznačen (čím sytější barva, tím vyšší
podobnost), odpovídající pasáž v nalezeném zdroji je pak u aktivního
úryvku zobrazena vpravo šedě. V záhlaví úryvku je zároveň uvedena přesná
procentní shoda. Dále zde najdeme číslo úryvku, tlačítko pro
(de)aktivaci daného úryvku a adresu nalezeného zdroje.


Způsob zobrazení textu dokumentu a jednotlivých úryvků lze pak upravovat
pomocí odpovídajících tlačítek na panelu ve střední části stránky, jak
bylo popsáno výše v sekci *Funkční panel*.



