Vztah s dalšími systémy MU
==========================

Hlavním nástrojem Masarykovy univerzity pro vyhledávání potenciálních
plagiátů je **aplikace „vejce vejci"** dostupná přímo v [Informačním
systému MU](https://is.muni.cz/){:target="_blank"}. Tato aplikace funguje na principu
vyhledávání podobností mezi texty, které jsou obsaženy v její databázi,
je ale schopna vyhledávat i možné podobnosti vůči zdrojům z celého
Internetu.

V souvislosti s plagiátorstvím vyvíjí a provozuje Masarykova univerzita
také následující veřejně přístupné systémy:

-   **[Theses.cz](https://theses.cz/){:target="_blank"}** jde o systém pro odhalování
    plagiátů mezi závěrečnými pracemi. Systém v současnosti využívá už
    několik desítek převážně českých vysokých škol a univerzit.
-   **[Odevzdej.cz](http://odevzdej.cz/){:target="_blank"}** jde o systém pro odhalování
    plagiátů v seminárních nebo jiných pracích. Jde o systém, který
    umožňuje kontrolu prací pro veřejnost.
-   **[Repozitar.cz](https://repozitar.cz/){:target="_blank"}** jde o repozitář vědeckých
    prací. Vůči vědeckým pracím nahraným do tohoto systému lze pomocí
    výše uvedených systémů rovněž kontrolovat práce na možné podobnosti.

Vedle výše uvedených možností pro kontrolu plagiátů nabízí Filozofická
fakulta Masarykovy univerzity navíc systém **Urkund**. Jde o mezinárodně
využívaný systém nezávislý na ostatních antiplagiátorských systémech
vyvíjených na MU (tj. oba systémy pracují s jinou databází zdrojů a s
jiným algoritmem pro jejich porovnávání).

Systém **Urkund** je využíván primárně pro kontrolu studentských prací
odevzdávaných v rámci e-learningového systému ELF na FF MU. Lze jej
ovšem využívat i samostatně, např. pro dodatečnou kontrolu závěrečných
prací či seminárních prací odevzdávaných mimo systém ELF.
