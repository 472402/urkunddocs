Založení učitelského účtu a práce v něm
=======================================

Na stránkách systému Urkund si učitelé Filozofické fakulty MU mohou
vytvářet vlastní účty a využívat pak systém Urkund i nad rámec ELFu.
Níže se dozvíte, jak si založit učitelský účet v systému Urkund, a
seznámíte se se základními způsoby jeho ovládání.







    1.  [**2.1** Práce se složkami](#TOC-Pr-ce-se-slo-kami)
    2.  [**2.2** Seskupování podle kurzů](#TOC-Seskupov-n-podle-kurz-)
    3.  [**2.3** Termín odevzdání](#TOC-Term-n-odevzd-n-)
    4.  [**2.4** Anonymní odevzdávání](#TOC-Anonymn-odevzd-v-n-)

### Vytvoření učitelského účtu

Založení učitelského účtu probíhá jiným způsobem, než u účtu
studentského. Učitelský účet si můžete založit na **[přihlašovací
stránce](https://secure.urkund.com/account/en-US/auth/login){:target="_blank"}**, na
kterou se z hlavní strany systému [Urkund](http://www.urkund.com/en/){:target="_blank"}
dostanete přes odkaz "Log in" (vlevo nahoře) a následně "Log in to
the URKUND system". Na přihlašovací stránce vpravo pak najdete sekci
pojmenovanou "Shibboleth Login" (viz obrázek níže), kde v nabídce
"Organization" vyberete položku **Masaryk University** a kliknete na
tlačítko pod nabídkou.

![](zalozeni-ucitelskeho-uctu-a-prace-v-nem/urkund_shibboleth_login.png)

Obr. 1: Přihlašení do systému Urkund pomocí učitelského účtu.


Následně budete přesměrování na standardní stránku **Jednotné přihlášení
na MUNI**, kde musíte zadat své *UČO* a *sekundární heslo*. Při prvním
přihlašování (tj. při vytváření účtu) budete muset ještě odsouhlasit, že
skutečně chcete se systémem Urkund sdílet požadované údaje, poté už bude
přihlašování probíhat standardně jen zadáváním uča a sekundárního
hesla.


Při vytváření učitelského účtu vám bude zároveň vytvořena speciální
adresa systému Urkund sloužící k analýze dokumentů zaslaných ke
kontrole. Tato adresa má podobu *UČO.muni@analysis.urkund.com* a jsou
na ni posílány všechny dokumenty, které chcete nechat kontrolovat
systémem Urkund. Tuto vlastní adresu můžete přitom používat jednak pro
kontrolu prací v systému ELFu (viz [Nastavení systému Urkund v
ELFu](/urkunddocs/nastaveni-systemu-urkund-v-elfu)), jednak při
používání systému Urkund mimo ELF prostředníctvím e-mailu (viz
[Fungování systému Urkund mimo
ELF](/urkunddocs/fungovani-systemu-urkund-mimo-elf)).

### Fungování učitelského účtu

Vytvořený učitelský účet bude zpočátku prázdný. Po odeslání prvních
souborů ke kontrole skrze adresu pro analýzu se odeslané soubory začnou
zobrazovat na stránce se seznamem dokumentů (viz obrázek níže).

![](zalozeni-ucitelskeho-uctu-a-prace-v-nem/urkund_ucitelsky_ucet.png)

Obr. 2: Ukázka seznamu kontrolovaných dokumentů v rámci učitelského účtu
v systému Urkund.


Na stránce seznamu kontrolovaných dokumentů můžete provádět několik
základních operací:

-   Sledovat stav nahraného dokumentu. Ikona vlevo ukazuje, zda je již
    analýza dokumentu hotová, nebo teprve probíhá.
-   Zobrazit si text zprávy, která byla odeslána spolu se souborem k
    analýze. V případě [používání systému Urkund mimo
    ELF](/urkunddocs/fungovani-systemu-urkund-mimo-elf) jde o text
    e-mailu, jehož přílohou byl odevzdaný soubor.
-   Podívat se na [skóre
    podobnosti](/urkunddocs/analyza-a-vysledky-kontroly#TOC-Sk-re-podobnosti)
    a [zobrazit si
    kompletní](/urkunddocs/analyza-a-vysledky-kontroly#TOC-Zpr-va-o-podobnosti)
    zprávu o podobnosti pro každý z dokumentů v seznamu.
-   Zobrazit si originální verzi kontrolovaného dokumentu kliknutím na
    jeho název.
-   Sledovat základní údaje o každém z dokumentů. Konkrétně jde o:
    velikost a počet slov, kdo dokument nahrál, kdy byl dokument nahrán
-   Vyhledávat konkrétní dokumenty pomocí vyhledávacího pole vpravo
    nahoře.
-   Nahrát hromadně větší množství souboru ke kontrole systémem Urkund
    pomocí tlačítka *Upload documents* vpravo nahoře.

Vedle těchto základních funkcí pak umožňuje učitelský účet v systému
Urkund i několik pokročilejších možností, jak s kontrolovanými dokumenty
pracovat. Všechny tyto možnosti jsou postaveny na využívání složek.

#### Práce se složkami

Odevzdané práce v učitelském účtu systému Urkund si můžete seskupovat do
tzv. složek. Jde o stejný princip jako při práci se složkami na běžném
PC. Novou složku můžete vytvořit kliknutím na tlačítko "New folder" v
prostřední části horního panelu (viz obr 2). Při vytváření složky musíte
zadat přinejmenším její název a *kód kurzu* (Course code). Zároveň ale
můžete využít i stanovení *termínu odevzdání* a funkce *anonymního
odevzdávání*.

![](zalozeni-ucitelskeho-uctu-a-prace-v-nem/urkund_nova_slozka.png)

Obr. 3: Vytváření složky v rámci učitelského účtu v systému Urkund.

#### Seskupování podle kurzů

U každé složky musíte specifikovat kód kurzu. Pomocí tohoto kódu pak
mohou být odevzdávané práce automaticky zařazovány do odpovídající
složky. Kód kurzu tudíž musí být unikátním řetězcem znaků, aby mohlo
přiřazení do složky proběhnout správně.

**Pozor.** Předměty obvykle probíhají opakovaně, s čímž je třeba počítat
při tvorbě složek. Pokud chcete mít zvláštní složku pro každý kurzu, je
třeba v nastavení složky jako kód kurzu zadat kombinaci skutečného kódu
předmětu podle ISu a označení semestru, ve kterém předmět probíhá. Např.
pokud je kód předmětu *FF1234*, lze jako kód kurzu v nastavení složky
použít *FF1234PS16*. Pokud vám stačí mít pro každý předmět jen jednu
složku, stačí zadat jen samotný kód předmětu.


Automatické zařazování odevzdaných prací do odpovídajících složek je
zajišťováno tím, že studenti uvádějí zadaný kód kurzu do předmětu
e-mailu, v jehož příloze posílají soubor se svým úkolem ke kontrole. Kód
kurzu přitom musí být uveden na začátku předmětu e-mailu a být uzavřen
do hranatých závorek (tedy např. *[FF1234]* či **[*FF1234PS16]*).

#### Termín odevzdání

Při vytváření složky může vyučující specifikovat datum odevzdání úkolu.
Po zaškrtnutí položky Deadline (viz obr 3) se zobrazí možnost zadat
přesné datum a čas pro odevzdávání. Zároveň můžete určit, zda mají být
úkoly odevzdané po stanoveném termínu rovněž kontrolovány systémem
Urkund. V tom případě budou studenti odevzdávající úkol po termínu o
této skutečnosti informováni e-mailem spolu s instrukcí kontaktovat
vyučujícího.


V seznamu dokumentů (viz obr 2) se bude zobrazovat červeně čas odevzdání
u těch úkolů, které byly odevzdány až po termínu.

#### Anonymní odevzdávání

Poslední možností, kterou můžete využít při práci se složkami je
anonymní odevzdávání. Pokud při vytváření složky zaškrtnete políčko
*Anonymous*, budou u všech nahraných dokumentů odstraněny všechny osobní
informace identifikující osobu, která soubor do systému nahrála. Tyto
informace jsou přitom odstraněny trvale, takže i při pozdějším manuálním
přesunutí souboru do jiné složky bez nastaveného anonymního odevzdání se
bude soubor stále zobrazovat bez osobních informací.

**Pozor.** Systém Urkund samozřejmě nedokáže skrýt případné osobní udaje
uvedené studentem přímo v e-mailu či samotném textu odevzdaného
dokumentu. Pokud chcete anonymní režim využívat, je proto třeba studenty
instruovat, že nemají nikde uvádět své jméno, UČO apod. (zvláště pozor
např. na automatický podpis v e-mailu či metadata v odevzdávaném
dokumentu).


