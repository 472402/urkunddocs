Co je to Urkund
===============

**Urkund** ([oficiální stránky](http://www.urkund.com/en/){:target="_blank"}) je systém na
**kontrolu plagiátů textů**. V ELFu je možné jej využít ke kontrole
studentských prací odevzdaných jako vlastní textové úkoly nebo jako
příspěvky do fóra. Učiteli se poté u každé odevzdané práce zobrazí
"skóre" podobnosti textu s jinými zdroji a zároveň podrobná analýza
celého dokumentu s vyznačenými problémovými pasážemi. Studentům je v
základním nastavení u vlastních prací dostupné základní hodnocení shody.
Urkund kontroluje práce jak oproti vlastní obsáhlé celosvětové databázi
textů, tak proti ostatním textům odevzdaných studenty jinde v ELFu.


Na rychlé představení systému Urkund v devadesáti vteřinách se můžete
podívat na následujícím videu (EN):



Základní oficiální příručky k systému Urkund (v angličtině):

-   [Urkund Plagiarism
    handbook](http://static.urkund.com/manuals/URKUND_Plagiarism_Handbook_EN.pdf){:target="_blank"}
-   [Quickstart Guide
    URKUND](http://static.urkund.com/manuals/URKUND_Userguide.pdf){:target="_blank"}
-   [An overview of the URKUND
    analysis](http://static.urkund.com/manuals/URKUND_Analysis_Quick_Reference_Sheet.pdf){:target="_blank"}
-   [The URKUND Web Inbox Quick Reference
    Sheet](http://static.urkund.com/manuals/URKUND_Web_Inbox_Quick_Reference_Sheet.pdf){:target="_blank"}


