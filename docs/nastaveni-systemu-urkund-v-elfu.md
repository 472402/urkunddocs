Nastavení systému Urkund v ELFu
===============================

Systém Urkund můžete v rámci ELFu používat celkem ve třech typech
aktivit. Jde o: [Úkol](http://moodledocs.phil.muni.cz/cinnosti/ukol){:target="_blank"},
[Fórum](http://moodledocs.phil.muni.cz/cinnosti/forum){:target="_blank"} a
[Workshop](http://moodledocs.phil.muni.cz/cinnosti/workshop){:target="_blank"}. Obecné
fungování a možnosti nastavení jednotlivých aktivit jsou popsány v
dokumentaci pod jednotlivými odkazy. Níže najdete návod, jak u těchto
aktivit pracovat s nastavením systému Urkund.







### Základní nastavení

Ve výchozím nastavení je nástroj Urkund u všech zmiňovaných typů aktivit
povolen a zapnutý (tj. Úkol, Fórum a Workshop). Pokud tedy nepotřebujete
využít specifického nastavení, stačí ponechat výchozí nastavení a systém
Urkund začít využívat.



Chcete-li fungování nástroje Urkund upravit, najdete potřebné nastavení
vždy v nastavení dané aktivity v sekci *Antiplagiátorský nástroj
Urkund*. Základní nastavení je stejné pro všechny uvedené typy aktivit.
Najdete zde následující položky:

1.  **Povolit Urkund.** První položkou ovlivňujete, zde chcete nástroj
    Urkund v rámci dané aktivity využívat (tj. zda se mají odevzdané
    práce nechávat kontrolovat na možné plagiátorství). Ačkoli není
    problém nechat nástroj Urkund povolen i v případě, že ho
    nevyužíváte, v některých případech může být vhodnější nástroj
    vypnut, pokud již předem víte, že ho nebudete v rámci dané aktivity
    využívat. Např. pokud využíváte nějaké diskuzní fórum pouze pro
    vzájemnou komunikaci, je vhodnější nástroj Urkund vypnout, aby
    studenti nebyli zmatení automaticky posílanými e-maily o kontrole
    jejich diskuzních příspěvků.
2.  **Adresa příjemce.** Ve druhé položce nastavení se zadává adresa
    systému Urkund pro kontrolu souborů. Pokud nemáte vlastní adresu
    systému Urkund (více o jejím získání najdete na stránce [Založení
    učitelského
    účtu](/urkunddocs/zalozeni-ucitelskeho-uctu-a-prace-v-nem)), je
    třeba zde ponechat výchozí adresu. Pokud máte vlastní adresu systému
    Urkund, můžete ji do tohoto pole zadat. Poté se vám budou všechny
    práce odevzdané v rámci daného úkolu zobrazovat ve vaší osobní
    složce na stránkách systému Urkund.
3.  **Ukázat studentům skóre podobnosti.** Zde můžete nastavit zda resp.
    kdy se studentům má zobrazit skóre podobnosti. Skóre podobnosti
    odpovídá procentu z textu odevzdaného úkolu, který byl identifikován
    jako shodný s jinými texty v databázi zdrojů systému Urkund. V
    nastavení můžete vybrat z možností: *Nikdy*, *Vždy* a *Po uzavření
    aktivity*. Ve výchozím nastavení je zvolena možnost *Vždy* a
    studentům se tedy skóre podobnosti zobrazuje ihned, jakmile je v
    systému dostupné.
4.  **Ukázat studentům zprávu o podobnosti.** Jedná se o obdobné
    nastavení jako výše, akorát se týká zobrazení podrobné zprávy o
    podobnosti. Ve výchozím nastavení se zpráva o podobnosti studentům
    nezobrazuje.
5.  **Poslat studentovi e-mail.** Poslední položka nastavení se týká
    toho, zda má být studentům automaticky posílán e-mail v okamžiku,
    kdy byl jimi odevzdaný soubor již zkontrolován a je k dispozici
    finální zpráva o podobnosti. Toto nastavení se týká skutečně jen
    upozornění na hotovou kontrolu souboru. Neovlivňuje tedy automatické
    posílání e-mailu upozorňujícího studenta ihned po odevzdání, že je
    jeho práce bude kontrolována systémem Urkund (posílání těchto
    informačních e-mailů nelze vypnout).

![](nastaveni-systemu-urkund-v-elfu/Urkund_zakladni_nastaveni.png)

Obr. 1: Základní nastavení antiplagiátorského nástroje Urkund.

### Rozšířené nastavení

Rozšířené nastvaení si zobrazíte kliknutím na tlačítko *Ukázat rozšířená
nastavení* v sekci *Antiplagiátorský nástroj Urkund*. Konkrétní položky
rozšířeného nastavení nástroje Urkund se mírně liší podle typu aktivity.
Celkově se v rozšířeném nastavení můžete setkat s následujícími
položkami:

-   **Odesílat soubory i online text.** V rámci aktivit typu Úkol a
    Fórum můžete specifikovat, zda se mají posílat ke kontrole v systému
    Urkund pouze nahrané soubory, nebo i text vepsaný přímo pomocí HTML
    editoru. V případě diskuzního fóra tak můžete rozlišit, zda chcete
    kontrolovat přímo text diskuzních příspěvků, nebo jen přiložené
    soubory, anebo diskuzní příspěvky i přiložené soubory. V případě
    aktivity typu Úkol je tato položka nastavení závislá na [Typech
    úkolů](http://moodledocs.phil.muni.cz/cinnosti/ukol/nastaveni-ukolu#TOC-Typy-kol-){:target="_blank"},
    které jsou v rámci nastavení povoleny (tj. zda studenti mohou
    odevzdávat pouze soubory, nebo i online text).
-   **Kdy má být soubor odeslán do nástroje Urkund.** U aktivit typu
    Úkol můžete ovlivnit, kdy má být nahraný soubor poslán ke kontrole
    systémem Urkund. Tato položka nastavení je opět závislá na jiném
    [Nastavení odevzdávání
    úkolů](http://moodledocs.phil.muni.cz/cinnosti/ukol/nastaveni-ukolu#TOC-Nastaven-odevzd-v-n-kol-){:target="_blank"}.
    Konkrétně jde o nastavení *Požadovat, aby studenti klikli na
    tlačítko Odeslat*, které vyžaduje, aby student odevzdání úkolu
    potvrdil kliknutím na odpovídající tlačítko. To umožňuje rozlišovat
    mezi pracovní verzí úkolu a tou, kterou student skutečně odeslal k
    hodnocení. Do nástroje Urkund pak mohou být posílány nahrané soubory
    okamžitě (tj. i pracovní verze úkolů), nebo až poté, co student
    soubor odešle k hodnocení.
-   **Povolit všechny podporované typy souborů.** U všech třech typů
    aktivit (tj. Úkol, Fórum a Workshop) můžete určit, zda mají být ke
    kontrole systémem Urkund posílány všechny podporované typy souborů,
    nebo jen vybrané.
-   **Typy souborů k odeslání.** Pokud chcete nechat kontrolovat jen
    vybrané typy souborů, musíte zde vybrat z nabídky dostupných typů,
    které chcete nechat kontrolovat.


