Založení studentského účtu
==========================

Studentský účet si můžete založit na **[stránce pro vytváření
studentských účtů](https://secure.urkund.com/account/account/create){:target="_blank"}**,
na kterou se dostanete z [přihlašovací
stránky](https://secure.urkund.com/account/en-US/auth/login){:target="_blank"} přes odkaz
*Create account for document upload (STUDENTS).* Případně můžete použít
odkaz, který je obsažen přímo v notifikačním e-mailu při odevzdávání
úkolů (viz [Odevzdávání úkolů ke
kontrole](/urkunddocs/odevzdavani-ukolu-ke-kontrole)).


Při vytváření účtu budete muset zadat *e-mailovou adresu*, *jméno* a
*heslo*. Poté Vám bude na zadanou adresu odeslán e-mail vyžadující
potvrzení pro vytvoření účtu. Po kliknutí na odkaz uvedený v doručeném
e-mailu vám bude účet úspěšně vytvořen a budete se moci přihlásit pomocí
emailové adresy a hesla, které jste zadali při vytváření účtu.


**Poznámka.** Chcete-li, aby se vám ve vytvořeném účtu shromažďovali
všechny práce odevzdávané v rámci ELFu, je třeba jako e-mailovou adresu
uvést vaši univerzitní adresu (tj. adresu ve formátu
*UČO@mail.muni.cz*). Váš účet v systému ELF totiž používá právě
univerzitní adresu.


Vytvořený studentský účet je zpočátku prázdný. Po odeslání nějakých
souborů ke kontrole skrze e-mailovou adresu účtu (případně skrze účet v
ELFu s touto emailovou adresou) se odeslané soubory budou zobrazovat na
stránce se seznamem dokumentů (viz obrázek níže).

![](zalozeni-studentskeho-uctu/urkund_studentsky_ucet.png)

Obr. 1: Ukázka seznamu kontrolovaných dokumentů v rámci studentského
účtu v systému Urkund.

Na stránce seznamu můžete především:

-   Sledovat stav nahraného dokumentu. Ikona vlevo ukazuje, zda je již
    analýza dokumentu hotová, nebo teprve probíhá.
-   Zobrazit si text zprávy, která byla odeslána spolu se souborem k
    analýze.
-   Vyřadit jednotlivé dokumenty z databáze zdrojů.
-   Zobrazit si originální verzi kontrolovaného dokumentu.
-   Vyhledávat konkrétní dokumenty pomocí vyhledávacího pole vpravo
    nahoře.


