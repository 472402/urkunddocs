Návody pro tvůrce kurzů: Urkund
===============================

V ELFu je možné využít antiplagiátorský systém Urkund ke kontrole
odevzdaných studentských prací. Na těchto stránkách naleznete hlavní
přehled dostupné nápovědy pro učitele i studenty.

### Základní přehled

Sekce **Základní přehled** uvádí úvodní postupy a základní východiska
práce učitele v systému ELF.

-   [**Co je to Urkund.**](/urkunddocs/co-je-to-urkund) Seznámení s
    fungováním systému Urkund a jeho využitím v e-learningovém systému
    ELF.
-   [**Plagiátorství na FF MU.**](/urkunddocs/plagiatorstvi-na-ff-mu)
    Základní východiska antiplagiátorské politiky na FF MU
-   [**Vztah s dalšími systémy
    MU.**](/urkunddocs/vztah-s-dalsimi-systemy-mu) Na MU lze využít více
    systémů na kontrolu plagiátorství. Zde najdete jejich základní
    přehled a vysvětlení vztahu se systémem Urkund.

### Práce v ELFu

Sekce **Práce v ELFu** obsahuje odkazy na podrobnou nápovědu pro učitele
a studenty týkající se práce se systémem Urkund přímo v rámci kurzů v
ELFu. Jedná se o:

-   [**Nastavení systému Urkund v
    ELFu.**](/urkunddocs/nastaveni-systemu-urkund-v-elfu) Podrobný návod
    pro učitele, jak si povolit a nastavit systém Urkund ve svých
    kurzech v ELFu.
-   [**Odevzdávání úkolů ke
    kontrole.**](/urkunddocs/odevzdavani-ukolu-ke-kontrole) Základní
    popis, jak probíhá odevzdávání kontrolovaných úkolů z pohledu
    studenta.
-   [**Analýza a výsledky
    kontroly.**](/urkunddocs/analyza-a-vysledky-kontroly) Přehled
    výsledků kontroly systémem Urkund.

### Práce mimo ELF

Sekce **Práce mimo ELF** obsahuje odkazy na podrobnou nápovědu pro
učitele a studenty, jak lze se systémem Urkund pracovat i mimo prostředí
ELFu.

-   [**Fungování systému Urkund mimo
    ELF.**](/urkunddocs/fungovani-systemu-urkund-mimo-elf) Základní
    vysvětlení, jak lze využívat Urkund nezávisle na ELFu.
-   [**Založení studentského
    účtu.**](/urkunddocs/zalozeni-studentskeho-uctu) Podrobný návod, jak
    si založit studentský účet na oficiálních stránkách systému Urkund.
-   [**Založení učitelského
    účtu.**](/urkunddocs/zalozeni-ucitelskeho-uctu-a-prace-v-nem)
    Podrobný návod, jak si založit učitelský účet na oficiálních
    stránkách systému Urkund.
