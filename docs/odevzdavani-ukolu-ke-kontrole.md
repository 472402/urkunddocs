Odevzdávání úkolů ke kontrole
=============================

Odevzdávání úkolů, které jsou následně kontrolovány antiplagiátorksým
nástrojem Urkund, probíhá vesměs standardním způsobem jako běžné
[odevzdávání
úkolů](http://moodledocs.phil.muni.cz/cinnosti/ukol/pouzivani-ukolu/odevzdat-soubor-y#TOC-Odevzd-v-n-){:target="_blank"}
nebo [přispívání do
fóra](http://moodledocs.phil.muni.cz/cinnosti/forum/pouzivani-fora#TOC-P-isp-v-n-do-f-ra){:target="_blank"}.
Při odevzdávání úkolu se akorát zobrazí upozornění v následujícím
znění:


*Všechny vložené soubory budou zkontrolovány antiplagiátorským nástrojem
Urkund.*

*Pokud si nepřejete, aby byl váš dokument dále používán jako jeden ze
zdrojů při budoucích antiplagiátorských analýzách mimo tento systém,
použijte odkaz k vyřazení dokumentu z databáze zdrojů, který se objeví
po dokončení analýzy.*



Jakmile student nahraje soubor a úkol odevzdá, systém Urkund mu zároveň
automaticky pošle upozornění (v angličtině). Předmět notifikačního
e-mailu má podobu "Confirmation of receipt - " následováno názvem
souboru, který student v rámci úkolu odevzdal (u diskuzních příspěvků a
úkolů typu online text je název souboru generován automaticky). Obsah
notifikačního e-mailu je pak tvoří následující:

-   Potvrzení, že systém Urkund přijal nahraný soubor ke zpracování. A
    to včetně přesného času, kdy k přijetí souboru došlo.
-   Výzva k registraci na stránkách systému Urkund (více viz stránku
    [Založení studentského
    účtu](/urkunddocs/zalozeni-studentskeho-uctu)). Registrace na
    stránkách systému Urkund však není pro kontrolu práce a fungování
    systému nutná a kontrola nahraného souboru proběhna nezávisle na
    tom, zda si v systému Urkund student vytvoří vlastní účet.
-   Nástin širšího kontextu problematiky plagiátorství a jejích různých
    dimenzí.
-   Základní popis fungování systému Urkund a obecného postupu při jeho
    využívání pro kontrolu studentských prací. Tento popis se netýká
    využívání systému Urkund v rámci ELFu, nýbrž obecného fungování mimo
    ELF (více viz stránku [Fungování systému Urkund mimo
    ELF](/urkunddocs/fungovani-systemu-urkund-mimo-elf)).
-   Upozornění na možnost vyřadit nahraný dokument z databáze zdrojů
    systému Urkund včetně odkazu, pomocí kterého může student vyřazení
    dokumentu provést.



V systému ELF je pak na stránce s odevzdaným souborem či vloženým
diskuzním příspěvkem viditelná ikona ozubeného kola (
![](odevzdavani-ukolu-ke-kontrole/Urkund_processing_icon.gif) - viz
obrázek níže) s popiskem: *Tento soubor byl odeslán ke kontrole do
antiplagiátorského nástroje Urkund. Nyní probíhá analýza.*


![](odevzdavani-ukolu-ke-kontrole/Urkund_probiha_analyza.png)

Obr. 1: Stránka Úkolu z pohledu studenta po odevzdání úkolu a odeslání
souboru ke kontrole.



**Pozor!** Samotná analýza nemusí probíhat ihned a může to trvat i v
řádu několika hodin, než se v ELFu zobrazí výsledné skóre podobnosti a
případně i zpráva o podobnosti. Doba od nahrání souboru po zobrazení
výsledků analýzy je proměnlivá a závisí především na délce
kontrolovaného textu a počtu dalších souborů, které jsou v té době
odeslány do systému Urkund ke kontrole.

