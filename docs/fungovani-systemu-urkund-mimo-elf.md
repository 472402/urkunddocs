Fungování systému Urkund mimo ELF
=================================

Ačkoli je systém Urkund používán na Filozofické fakultě MU primárně ve
spojení s e-learningovým systémem ELF, mohou jej učitelé a studenti
využívat i samostatně. V takovém případě pak probíhá celý proces
odevzdávání práce a její kontroly prostřednictvím e-mailu.


Pokud chcete jako vyučující používat systém Urkund i mimo ELF, je třeba
si nejprve na stránkách systému Urkund **založit učitelský účet** (viz
[Založení učitelského
účtu](/urkunddocs/zalozeni-ucitelskeho-uctu-a-prace-v-nem)). Tím získáte
adresu systému Urkund sloužící k analýze dokumentů zaslaných ke
kontrole. Tato adresa bude mít podobu *UČO.muni@analysis.urkund.com*.


Jakmile máte adresu systému Urkund pro analýzu dokumentů, probíhá
obvykle využívání systému prostřednictvím e-mailu následovně:

1.  Jakožto vyučující **informujete studenty, jak bude proces
    odevzdávání prací probíhat**, a že jejich práce budou kontrolovány
    antiplagiátorským systémem Urkund. V tomto kroku je vhodné studentům
    rovněž říct, v jakém formátu mají práci odevzdat. Systém Urkund je
    přitom schopen zpracovat běžné formáty jako .doc, .docx, .pdf, .txt,
    .rtf, .odt, .ppt, .pptx, .htm, .html, .wps, .Pages, .sxw. Může být
    také užitečné instruovat studenty, jak mají soubor pojmenovat (z
    důvodu pozdějšího snadného dohledání).
2.  Zároveň **sdělíte studentům vaši adresu systému Urkund pro analýzu
    dokumentů**, na kterou vám mají zasílat hotové práce.
3.  Následně studenti pracují na zadaném úkolu. Jakmile pak mají
    výsledný text připraven k odevzdání, mohou studenti přistoupit k
    **odevzdání úkolu v podobě zaslání e-mailu s přílohou na adresu pro
    analýzu dokumentů**, kterou jim vyučující sdělil. Samotný text úkolu
    tedy studenti přiloží k e-mailu v podobně samostatného souboru
    jakožto přílohu.
4.  Jakmile studenti práci pomocí e-mailu s přílohou odevzdají, **systém
    Urkund informuje studenty o tom, že jejich práce byla v pořádku
    přijata**. Od tohoto okamžiku může vyučující vidět odevzdanou práci
    po přihlášení do svého účtu na stránkách systému Urkund.
5.  Poté **systém Urkund provádí analýzu odevzdaného souboru**. Samotná
    analýza by obvykle měla být hotová cca do jedné hodiny. V závislosti
    na délce kontrolovaného textu a počtu dalších souborů ke kontrole se
    však tato doba může prodloužit i na několik hodin. Vždy by ale měla
    být analýza hotová nejpozději do 24 hodin.
6.  V okamžiku, kdy je analýza odevzdaného souboru hotová, **systém
    Urkund pomocí e-mailu informuje vyučujícího, že jsou k dispozici
    výsledky analýzy**. Zpráva je vyučujícímu doručena na jeho běžný
    univerzitní e-mail (tj. UČO@mail.muni.cz), součástí zprávy je pak
    odkaz na kompletní zprávu o podobnosti (pro seznámení se strukturou
    a fungováním zprávy viz [Zpráva o
    podobnosti](/urkunddocs/analyza-a-vysledky-kontroly#TOC-Zpr-va-o-podobnosti)).

**Poznámka.** Přístup ke všem odevzdaným pracím i k jejich analýzám má
vyučující samozřejmě i přes svůj učitelský účet na stránkách systému
Urkund. Po vytvoření adresy pro analýzu však může celý proces
odevzdávání prací a jejich kontroly probíhat přímo prostřednictvím
e-mailů a bez nutnosti přihlašovat se do systému Urkund. Pro odevzdání
práce pomocí e-mailu si studenti účet v systému Urkund vytvářet nemusí.
Pokud ale chtějí mít přístup ke všem svým kontrolovaným souborům mohou
si vytvořit studentský účet (viz [Založení studentského
účtu](/urkunddocs/zalozeni-studentskeho-uctu)).

