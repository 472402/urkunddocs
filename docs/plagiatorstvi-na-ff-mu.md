Plagiátorství na FF MU
======================

Za plagiátorství se dle
[TDKIV](http://aleph.nkp.cz/F/?func=direct&doc_number=000014609&local_base=KTD){:target="_blank"}
považuje ***vydávání cizího literárního nebo jiného uměleckého nebo
vědeckého díla za vlastní, popř. převzetí části cizí práce, bez uvedení
použitých zdrojů***.

V kontextu vysokoškolských studentských seminárních a diplomových prací
se tak za plagiátorství považují především takové případy, kdy student:

-   vydává cizí seminární či diplomovou práci za svou,
-   využívá ve své práci části cizího textu, které vydává za své,
-   opomíjí (i neúmyslně) citovat zdroje, které ve své práci využívá,
-   cituje zdroje nepřesně, čímž zabraňuje jejich dohledání.
    (více viz stránka
    [Plagiátorství](https://www.muni.cz/study/plagiatorstvi?lang=cs){:target="_blank"} na
    webu MU)

Filozofická fakulta Masarykovy univerzity posuzuje plagiátorství dle [§
68 zákona o vysokých
školách](http://zakony.centrum.cz/zakon-o-vysokych-skolach/cast-6-paragraf-68){:target="_blank"}
jako **disciplinární přestupek**, za který může být udělena sankce od
**napomenutí**, přes **podmíněné vyloučení** ze studia, až po
nepodmíněné **vyloučení ze studia**, je-li zřejmé, že disciplinární
přestupek byl spáchán úmyslně (viz [Disciplinární řád FF
MU](https://www.muni.cz/phil/general/legal_standards/disciplinary_code){:target="_blank"}).

Některé ústavy a katedry FF MU pak mohou mít svoji antiplagiátorskou
politiku specifikovánu podrobněji. Informace jsou pak obvykle k nalezení
na webových stránkách daného ústavu či katedry (viz např. [Katedra
anglistiky a
amerikanistiky](http://www.phil.muni.cz/wkaa/home/vyveska/plagiatorstvi){:target="_blank"}
či [Kabinet informačních studií a
knihovnictví](http://kisk.phil.muni.cz/dulezite-dokumenty/studium/antiplag_politika_kisk.pdf){:target="_blank"}).
